//
//  HerdManagerApp.swift
//  Shared
//
//  Created by Franz Murner on 20.03.22.
//

import SwiftUI

@main
struct HerdManagerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
